package com.mcfadyen.challenge.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = McFadyenChallengeApplication.class)
public class McFadyenChallengeApplicationTests {

    @Test
    public void contextLoads() {
    }

}
