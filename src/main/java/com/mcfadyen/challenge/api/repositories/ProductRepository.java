package com.mcfadyen.challenge.api.repositories;

import com.mcfadyen.challenge.api.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {

}
