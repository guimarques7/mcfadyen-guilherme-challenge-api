package com.mcfadyen.challenge.api.controllers;

import com.mcfadyen.challenge.api.models.CommerceItem;
import com.mcfadyen.challenge.api.models.Shoppingcart;
import com.mcfadyen.challenge.api.services.impl.ShoppingcartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class ShoppingcartController {

    @Autowired
    private ShoppingcartServiceImpl shoppingCartService;

    /**
     * Lists the current Shoppingcart in the Session
     *
     * @return
     */
    @GetMapping("/v1/shoppingcart")
    Shoppingcart shoppingcartGet(){
        return shoppingCartService.getCurrentShoppingcart();
    }

    /**
     * Inserts or increments a item in the current Shoppingcart
     *
     * @param commerceItem
     * @return
     */
    @PostMapping("/v1/shoppingcart/items")
    CommerceItem shoppingcartItemsPost(@RequestBody CommerceItem commerceItem){
        return shoppingCartService.addItem(commerceItem);
    }

    /**
     * Deletes a CommerceItem from the Shoppingcart based on Id
     *
     * @param id
     * @return
     */
    @DeleteMapping("/v1/shoppingcart/item/{id}")
    void shoppingcartItemsIdDelete(@PathVariable("id") String id){
        shoppingCartService.deleteItem(id);
    }
}
