package com.mcfadyen.challenge.api.controllers;

import com.mcfadyen.challenge.api.models.Product;
import com.mcfadyen.challenge.api.services.impl.ProductServiceImpl;
import com.mcfadyen.challenge.api.services.impl.ShoppingcartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private ShoppingcartServiceImpl shoppingCartService;

    /**
     * Lists all the products
     *
     * @return List<Product>
     */

    @GetMapping("/v1/products")
    List<Product> productGet() {
        return productService.getProducts();
    }
}
