package com.mcfadyen.challenge.api.models;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class Shoppingcart implements Serializable {
    private List<CommerceItem> items;
    private BigDecimal amount;

    public Shoppingcart() {
        this.items = new ArrayList<CommerceItem>();
        this.amount = BigDecimal.ZERO;
    }

    public Shoppingcart(List<CommerceItem> items, BigDecimal amount) {
        this.items = items;
        this.amount = amount;
    }

    public List<CommerceItem> getItems() {
        return items;
    }

    public void setItems(List<CommerceItem> items) {
        this.items = items;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
