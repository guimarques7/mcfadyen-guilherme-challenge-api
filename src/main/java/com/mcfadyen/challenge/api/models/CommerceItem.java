package com.mcfadyen.challenge.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;


@Component
@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class CommerceItem implements Serializable {

    private String id;
    @JsonProperty("product_id")
    private String productId;
    private Integer quantity;
    private BigDecimal amount;

    public CommerceItem() {
    }

    public CommerceItem(String id, String productId, Integer quantity, BigDecimal amount) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
