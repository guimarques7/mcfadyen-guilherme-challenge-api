package com.mcfadyen.challenge.api.services.impl;

import com.mcfadyen.challenge.api.models.CommerceItem;
import com.mcfadyen.challenge.api.models.Product;
import com.mcfadyen.challenge.api.models.Shoppingcart;
import com.mcfadyen.challenge.api.services.HttpSessionService;
import com.mcfadyen.challenge.api.services.ShoppingcartService;
import com.mcfadyen.challenge.api.services.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service("shoppingCartService")
public class ShoppingcartServiceImpl implements ShoppingcartService {

    @Autowired
    HttpSessionService httpSessionService;
    @Autowired
    ProductServiceImpl productServiceImpl;

    @Override
    public Shoppingcart getCurrentShoppingcart() {
        return httpSessionService.getCurrentShoppingcart();
    }

    @Override
    public CommerceItem addItem(CommerceItem commerceItem){
        Shoppingcart currentShoppingcart = httpSessionService.getCurrentShoppingcart();

        List<CommerceItem> shoppingCartCommerceItems = currentShoppingcart.getItems();

        if (shoppingCartCommerceItems == null || shoppingCartCommerceItems.isEmpty()) {
            shoppingCartCommerceItems = new ArrayList<CommerceItem>();
            currentShoppingcart.setItems(shoppingCartCommerceItems);
        }

        if (isProductValid(commerceItem.getProductId())) {
            addProductToCart(commerceItem.getProductId(), commerceItem.getQuantity());

            refreshCartAmount();
        }

        httpSessionService.updateShoppingcart(currentShoppingcart);

        return commerceItem;
    }

    @Override
    public void deleteItem(String id) {
        Shoppingcart currentShoppingcart = httpSessionService.getCurrentShoppingcart();
        List<CommerceItem> shoppingCartCommerceItems = currentShoppingcart.getItems();
        if (shoppingCartCommerceItems == null || shoppingCartCommerceItems.isEmpty()) {
            return;
        }

        for (CommerceItem commerceItem : shoppingCartCommerceItems) {
            if (id.equals(commerceItem.getProductId())) {
                shoppingCartCommerceItems.remove(commerceItem);
                currentShoppingcart.setItems(shoppingCartCommerceItems);
                refreshCartAmount();
                httpSessionService.updateShoppingcart(currentShoppingcart);
                return;
            }
        }
    }

    /**
     * Recalculates the order/cart amount
     */
    private void refreshCartAmount() {
        Shoppingcart currentShoppingcart = httpSessionService.getCurrentShoppingcart();
        List<CommerceItem> shoppingCartCommerceItems = currentShoppingcart.getItems();

        BigDecimal cartAmount = BigDecimal.ZERO;
        if (shoppingCartCommerceItems != null && !shoppingCartCommerceItems.isEmpty()) {
            for (CommerceItem commerceItem : shoppingCartCommerceItems) {
                cartAmount = cartAmount.add(commerceItem.getAmount());
            }
        }

        currentShoppingcart.setAmount(cartAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN));
    }

    /**
     * Inserts the new item in cart or just increase the quantity
     *
     * @param productId
     * @param quantity
     */
    private void addProductToCart(String productId, Integer quantity) {
        Shoppingcart currentShoppingcart = httpSessionService.getCurrentShoppingcart();
        List<CommerceItem> shoppingCartCommerceItems = currentShoppingcart.getItems();
        Product product = productServiceImpl.getById(productId);
        boolean alreadyInCart = false;

        for (CommerceItem commerceItem : shoppingCartCommerceItems) {
            if (productId.contentEquals(commerceItem.getProductId())) {
                if( (commerceItem.getQuantity() + quantity ) <= 0)
                    commerceItem.setQuantity(0);
                if( (commerceItem.getQuantity() + quantity ) > 0)
                    commerceItem.setQuantity(commerceItem.getQuantity() + quantity);
                commerceItem.setAmount(calculateCommerceItemAmount(product, commerceItem.getQuantity()));
                if(commerceItem.getQuantity() == 0)
                    shoppingCartCommerceItems.remove(commerceItem);
                alreadyInCart = true;
                break;
            }
        }

        if (!alreadyInCart) {
            shoppingCartCommerceItems
                    .add(new CommerceItem(UUID.randomUUID().toString(), product.getId(), quantity, calculateCommerceItemAmount(product, quantity)));
        }

        currentShoppingcart.setItems(shoppingCartCommerceItems);
    }

    /**
     * Calculates the CommerceItem amount based on his quantity
     *
     * @param quantity
     * @param product
     * @return BigDecimal
     */
    private BigDecimal calculateCommerceItemAmount(Product product, Integer quantity) {
        BigDecimal productPrice = product.getPrice();
        BigDecimal commerceItemQuantity = new BigDecimal(quantity);

        return productPrice.multiply(commerceItemQuantity).setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

    /**
     * Verifies if the productId exists
     *
     * @param productId
     * @return boolean
     */
    private boolean isProductValid(String productId) {

        if (! (productId.isEmpty() || productId.equals(null))) {
            return productServiceImpl.getById(productId) != null;
        }

        return false;
    }

}