package com.mcfadyen.challenge.api.services;

import javax.servlet.http.HttpSession;

import com.mcfadyen.challenge.api.models.Shoppingcart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("httpSessionService")
public class HttpSessionService {

    @Autowired
    HttpSession httpSession;

    final String SHOPPING_CART = "shoppingCart";

    /**
     * Lists the current Shoppingcart in the Session
     *
     * @return Shoppingcart
     */
    public Shoppingcart getCurrentShoppingcart(){
        if(httpSession.getAttribute(this.SHOPPING_CART) == null){
            updateShoppingcart(new Shoppingcart());
        }
        Shoppingcart shoppingcart = (Shoppingcart) httpSession.getAttribute(this.SHOPPING_CART);
        return shoppingcart;
    }

    /**
     * Updates the current Shoppingcart in the Session
     *
     * @param shoppingcart
     */
    public void updateShoppingcart(Shoppingcart shoppingcart){
        httpSession.setAttribute(this.SHOPPING_CART, shoppingcart);
    }
}