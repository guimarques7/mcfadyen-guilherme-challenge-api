package com.mcfadyen.challenge.api.services;

import com.mcfadyen.challenge.api.models.Product;
import java.util.List;

public interface ProductService {

    /**
     * Lists all the Products
     *
     * @return List<Product>
     */
    List<Product> getProducts();

    /**
     * Lists a Product by Id
     *
     * @param id
     *
     * @return Product
     */
    Product getById(String  id);
}