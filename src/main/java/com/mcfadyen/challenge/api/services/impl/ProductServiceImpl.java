package com.mcfadyen.challenge.api.services.impl;

import java.util.List;

import com.mcfadyen.challenge.api.models.Product;
import com.mcfadyen.challenge.api.repositories.ProductRepository;
import com.mcfadyen.challenge.api.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("productService")
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> getProducts(){
        return productRepository.findAll();
    }
    @Override
    public Product getById(String id) {
        return productRepository.findById(id).get();
    }

}
