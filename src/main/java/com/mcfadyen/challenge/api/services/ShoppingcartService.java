package com.mcfadyen.challenge.api.services;

import com.mcfadyen.challenge.api.models.CommerceItem;
import com.mcfadyen.challenge.api.models.Shoppingcart;

public interface ShoppingcartService {

    /**
     * Lists the current Shoppingcart in the Session
     *
     * @return Shoppingcart
     */
    Shoppingcart getCurrentShoppingcart();

    /**
     * Inserts or increments a CommerceItem ins the current Shoppingcart
     *
     * @param commerceItem
     *
     * @return CommerceItem
     */
    CommerceItem addItem(CommerceItem commerceItem);

    /**
     * Deletes a CommerceItem from the Shoppingcart based on Id
     *
     * @param id
     */
    void deleteItem(String id);

}