package com.mcfadyen.challenge.api;

import com.mcfadyen.challenge.api.models.Product;
import com.mcfadyen.challenge.api.repositories.ProductRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.UUID;

@SpringBootApplication
public class McFadyenChallengeApplication implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ProductRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(McFadyenChallengeApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if(repository.findAll().size() > 0)
            return;
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 01", "./img/prato-especial.jpg", new BigDecimal(28.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 02", "./img/prato-comum.jpg", new BigDecimal(17.50))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 03", "./img/especialidade-da-casa.jpg", new BigDecimal(47.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 04", "./img/prato-especial.jpg", new BigDecimal(12.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 05", "./img/prato-comum.jpg", new BigDecimal(14.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 06", "./img/especialidade-da-casa.jpg", new BigDecimal(21.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 07", "./img/prato-especial.jpg", new BigDecimal(20.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 08", "./img/prato-comum.jpg", new BigDecimal(16.80))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 09", "./img/especialidade-da-casa.jpg", new BigDecimal(44.90))));
        logger.info("Inserting -> {}", repository.save(new Product(UUID.randomUUID().toString(), "Prato 10", "./img/prato-especial.jpg", new BigDecimal(36.90))));
    }

}
