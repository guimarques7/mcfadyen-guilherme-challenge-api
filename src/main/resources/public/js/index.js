var app = window.app || {};

'use strict';

app.init = function(cart){
	// totalItems totalAmount
	var total = 0,
	items = 0;

	if(undefined != cart.items && cart.items != null && cart.items != '' && cart.items.length > 0){
		_.forEach(cart.items, function(n, key) {
			items = (items + n.quantity);
			total = total  + (n.quantity * n.price);
		});

	}

	document.getElementById('totalItems').innerHTML = cart.items.length;
	var cartIcon = document.getElementsByClassName('cart-icon')[0];
	cartIcon.addEventListener("mouseenter", function(){
		var item = document.getElementsByClassName('cart')[0];
        item.style.display = 'block';
		item.style.width = '250px';
        item.style.left = '-270px';
	});
	cartIcon.addEventListener("mouseleave", function(){
        var item = document.getElementsByClassName('cart')[0];
        item.style.width = '0px';
        item.style.left = '0px';
        item.style.display = 'none';
    });

}

app.createProducts = function(){
	var success = function(res){
		var content = '',
		products = res;

		for(var i = 0; i < products.length; i++){
			content+= '<div class="coin-wrapper">'
			content+= '		<img src="'+products[i].image+'" alt="'+products[i].name+'">'
			content+= '		<span class="large-12 columns product-details">'
			content+= '			<h3>'+products[i].name+'</h3>'
			content+= '			<h4> <span class="price">$ '+products[i].price+'</span></h4>'
			content+= '		</span>'
			content+= '		<a class="large-12 columns btn submit ladda-button prod-'+products[i].id+'" data-style="slide-right" onclick="app.addtoCart(\''+products[i].id+'\');">Add to basket</a>'
			content+= '		<div class="clearfix"></div>'
			content+= '</div>';
		}

		document.getElementById("productsWrapper").innerHTML = content;

		localStorage.setItem('products',JSON.stringify(products));
	}
	app.apiRequest('GET', 'products', null, success);
}

app.addtoCart = function(id){
	var l = Ladda.create( document.querySelector('.prod-'+id));
	l.start();
	var products = JSON.parse(localStorage.getItem('products')),
	product = _.find(products,{ 'id' : id });
	if(undefined != product){
		var data = {
			product_id: product.id,
			quantity: 1
		};
		var success = function(){
			l.stop();
			app.apiRequest('GET', 'shoppingcart', null, function(res){
				app.getProducts(res);
			});
		}
		app.apiRequest('POST', 'shoppingcart/items', data, success);
	}else{
		alert('Error! Try again later.');
	}
}

app.removeOneFromCart = function(event, id){
    var products = JSON.parse(localStorage.getItem('products')),
        product = _.find(products,{ 'id' : id });
    if(undefined != product){
        setTimeout(function(){
            var data = {
                product_id: product.id,
                quantity: -1
            };
            var success = function(){
                app.apiRequest('GET', 'shoppingcart', null, function(res){
                    app.getProducts(res);
                });
            };
            app.apiRequest('POST', 'shoppingcart/items', data, success);
        },50);
    }else{
        alert('Error! Try again later.');
    }
}

app.deleteItem = function(event, id){
    var products = JSON.parse(localStorage.getItem('products')),
        product = _.find(products,{ 'id' : id });
    if(undefined != product){
        setTimeout(function(){
            var success = function(){
                app.apiRequest('GET', 'shoppingcart', null, function(res){
                    app.getProducts(res);
                });
            };
            app.apiRequest('DELETE', 'shoppingcart/item/'+product.id, null, success);
        },50);
    }else{
        alert('Error! Try again later.');
    }
}

app.getProducts = function(cart){
	var wrapper = document.getElementsByClassName('cart'),
	total = 0;

	wrapper[0].innerHTML = "";

	if(undefined == cart || null == cart || cart == '' || cart.items.length == 0){
		wrapper[0].innerHTML += '<li>Your basket is empty</li>';
	}else{
		var items = '';

		_.forEach(cart.items, function(n, key) {
			success = function (prod) {
                items += '<li>'
                items += '<div class="thumbnail"><img src="'+prod.image+'" /></div>'
                items += '<h3 class="title">'+prod.name+'<br><span class="price">'+n.quantity+' x $ '+prod.price+' USD</span> <button class="add" onclick="app.removeOneFromCart(event, \''+prod.id+'\')"><i class="icon ion-minus-circled"></i></button> <button onclick="app.deleteItem(event, \''+prod.id+'\')" ><i class="icon ion-close-circled"></i></button><div class="clearfix"></div></h3>'
                items += '</li>'
            }
            app.findProductById(n.product_id, success);
			total = total + n.amount;
		});

		items += '<li id="total">Total : $ '+total+' USD <div id="submitForm"></div></li>';
		wrapper[0].innerHTML = items;
	}
    document.getElementById('totalItems').innerHTML = cart.items.length;
}

app.findProductById = function(id, callback){
    var products = JSON.parse(localStorage.getItem('products'));
    _.forEach(products, function(n, key) {
    	if(n.id == id)
    		return callback(n);
    });
}

app.apiRequest = function(method, url, data, callback){
	var uri = '/v1/',
		r;

	try {
		r = new window.XMLHttpRequest();
	} catch ( e ) {}
	r.open(method, uri + url, true);
	r.onreadystatechange = function () {
		if (this.readyState == 4 && (this.status == 200 || this.status == 201)) {
			if(this.responseText.length > 0)
				return callback(JSON.parse(this.responseText));
			return callback();
		}
        if (this.readyState == 4 && (this.status != 200 && this.status != 201)) {
            return console.log(this);
        }
	}
	if(method == 'POST')
		r.setRequestHeader("Content-Type", "application/json");

	return r.send(JSON.stringify(data));
}

function ready(fn) {
    if (document.readyState != 'loading'){
        fn();
    } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        document.attachEvent('onreadystatechange', function() {
            if (document.readyState != 'loading')
                fn();
        });
    }
}

ready(function() {
	app.apiRequest('GET', 'shoppingcart', null, function(res){
		app.init(res);
        app.getProducts(res)
	});
	app.createProducts();
});